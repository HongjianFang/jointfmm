#!/usr/bin/env python
########################################################################################
# Script to run joint inversion with body wave and surface wave data
# Hongjian Fang @ USTC 2016.11.11
########################################################################################
#
import os
from shutil import copyfile
import sys
#
# forward
fmm='fm3d'
frech='frechgen'
ttim='arrivals.dat'
cvg='vgrids.in'
cig='interfaces.in'
csl='sources.in'
fmmout='fm3dlog.out'
# surface wave
#########################################################
surfdirect='surfdirect'
mtravsurf='mtimessurf.dat'
ttimessurf='ttimessurf.dat'
diagnossurf='surfdirect.out'
#
# inversion 
inv='invert3d'
invinput = 'invert3d.in'
itn='inviter.in'
mtrav='mtimes.dat'
rtrav='rtimes.dat'
ivg='vgridsref.in'
iig='interfacesref.in'
isl='sourcesref.in'
#
# residuals
resid='residuals'
resout='residuals.dat'
#
###############################
ifile='tomo3d.in'
with open(ifile, 'r') as fp:
    lines = fp.readlines()
NI = int(lines[0].strip())
BGITER = int(lines[1].strip())
BINV = int(lines[2].strip())
JOINT = int(lines[3].strip())
velinv = 1
###############################
###############################
#
# If necessary, copy the initial velocity,
# interface and source files to the current files
# read by fm3d. Generate the frechet.in file and
# set the iteration number to 1.
#
def rearrangefrechet():
      with open('otimes.dat','r') as otime:
      	nd = int(otime.readline())		  
    	srcidx = np.zeros(nd,)
      	for ii in range(nd):
     	  lns = otime.readline().split()
    	  srcidx[ii] = int(lns[6])
      with open('vgrids.in','r') as vgrids:
         vgrids.readline()
         nx,ny,nz =[int(ii) for ii in vgrids.readline().split()]
      ngrid = nx*ny*nz*2
  	  ds = Dataset('frechet.nc')
  	  nrow1 = ds.dimensions.values()[0].size 
  	  nrowseg = np.zeros((nrow1,),dtype=int)
  	  nrowseg = ds.variables['Non_row'][:]
  	  nzero1 =ds.dimensions.values()[1].size 
  	  nzeroidseg = np.zeros((nzero1,),dtype=int)
  	  nzeroidseg = ds.variables['Non_id'][:]
  	  nzeroseg = np.zeros((nzero1,),dtype=float)
  	  nzeroseg = ds.variables['Non_value'][:]
      idx = 0
      #nzeroid = np.zeros((nzero1,),dtype=int)
      nzeroid = nzeroidseg
      for ii in range(nrow1):
          for jj in range(nrowseg[ii]):
              tmp = nzeroidseg[jj]
              idx += 1
              if tmp > ngrid:
                  nzeroid[idx-1] = ngrid+np.mod(tmp-1-ngrid,4)+1+4*srcidx[ii] 
      ds.close()    

      dsnew = Dataset('frechet.nc','w',format='NETCDF3_CLASSIC')
      nrowdim = dsnew.createDimension('nrow',nrow1)
      nzerodim = dsnew.createDimension('nzero',nzero1)
      ncnrow = dsnew.createVariable('Non_row',np.int32,('nrow',))
      ncnzero = dsnew.createVariable('Non_value',np.float32,('nzero',))
      ncnzero_id = dsnew.createVariable('Non_id',np.int32,('nzero',))
      ncnrow[:] = nrowseg
      ncnzero[:] = nzeroseg
      ncnzero_id[:] = nzeroid
      dsnew.close()
      return 


from datetime import datetime
start_time = datetime.now()
if BGITER == 0:
    copyfile(ivg,cvg)
    copyfile(iig,cig)
    copyfile(isl,csl)
    copyfile('receiversori.in','receivers.in')
    ITER=1
    with open(itn, 'w') as fp:
        fp.write(str(ITER))
    with open(invinput,'r') as fp:
        data = fp.readlines()
    data[22] = lines[ITER+3].split('|')[0].strip()+'\n'
    data[23] = lines[ITER+3].split('|')[1].strip()+'\n'
    data[24] = lines[ITER+3].split('|')[2].strip()+'\n'
    data[30] = lines[3]
    velinv = int(data[22].split()[0])
    eqloc = int(data[24].split()[0])
    with open(invinput,'w') as fp:
        fp.writelines(data)

    os.system(frech)
#
#
# Run FMM
#
if BGITER==0 or (BGITER==1 and BINV==1 ):
  if(JOINT==0 or JOINT==2):
    os.system(fmm+'> '+fmmout)
    if eqloc == 1:
        rearrangefrechet()
    copyfile(ttim,mtrav)
    copyfile(ttim,rtrav)
    os.system(resid+'> '+resout)
  if(JOINT==1 or (JOINT==2 and velinv ==1)):
    os.system(surfdirect+' > '+diagnossurf)
    copyfile(ttimessurf,mtravsurf)

try:
    with open(itn,'r') as inviter:
        ITER = int(inviter.readline())
except:
    ITER = 1
#
# Now begin a loop to iteratively apply subspace inversion
# and FMM
#
while ITER<=NI:
    copyfile('receivers.in','receiversref.in')
    os.system(inv)

    runid = sys.argv[1]
    iterdir = runid+'/iteration'+str(ITER).zfill(3)
    if not os.path.exists(iterdir):
    	os.makedirs(iterdir)
    files = ['interfaces.in', 'vgrids.in', 'sources.in',\
	      'mtimes.dat','mtimessurf.dat','invert3d.in','inviter.in','vgrids.invpvs']
    for line in files:
        copyfile(line,iterdir+'/'+line)

    with open(invinput,'r') as fp:
        data = fp.readlines()
    data[22] = lines[ITER+4].split('|')[0].strip()+'\n'
    data[23] = lines[ITER+4].split('|')[1].strip()+'\n'
    data[24] = lines[ITER+4].split('|')[2].strip()+'\n'
    data[30] = lines[3]
    velinv = int(data[22].split()[0])
    with open(invinput,'w') as fp:
        fp.writelines(data)

    if(JOINT==0 or JOINT==2):
      os.system(frech)
      os.system(fmm+'> '+fmmout)
      if eqloc == 1:
           rearrangefrechet()
      copyfile(ttim,mtrav)
      os.system(resid+'>> '+resout)
    if(JOINT==1 or (JOINT==2 and velinv ==1)):
      os.system(surfdirect+' > '+diagnossurf)
      copyfile(ttimessurf, mtravsurf)
    ITER=ITER+1
    COUNT=ITER

    with open(itn, 'w') as fp:
        fp.write(str(COUNT))

copyfile('vgrids.in',runid+'/vgrids.in')
copyfile('vgrids.invpvs',runid+'/vgrids.invpvs')
copyfile('tomo3d.in',runid+'/tomo3d.in')
copyfile('invert3d.in',runid+'/invert3d.in')
copyfile('residuals.dat',runid+'/residuals.dat')
end_time = datetime.now()
print 'Duration: {}'.format(end_time-start_time)
copyfile('screenout.txt',runid+'/screenout.txt')
